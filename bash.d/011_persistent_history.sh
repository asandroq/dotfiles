#!/bin/bash
#____________________________________________________________________________
#
# FILE:         011_persistent_history.sh
#
# DESCRIPTION:  Code related to persistent history functionality across all
#               open shells
#
#____________________________________________________________________________

log_bash_persistent_history()
{
   if [[ ${PHIST_OFF_RECORD:-0} -ne 0 ]]; then
      unset PHIST_METADATA
      return
   fi
   local LAST_RC=${LAST_RC:-$?}
   local LAST_PIPESTATUS=(${LAST_PIPESTATUS[@]:-${PIPESTATUS[@]}})
   # echo ${PIPESTATUS[@]}
   # echo ${LAST_PIPESTATUS[@]}
   local HISTTIMEFORMAT="%Y-%m-%d %T %z "
   [[
      $(history 1) =~ ^\ *[0-9]+\ +([^\ ]+\ [^\ ]+\ [^\ ]+)\ +(.*)$
   ]]
   local date_part="${BASH_REMATCH[1]}"
   local command_part="${BASH_REMATCH[2]}"

   # Prevent repetition if $PHIST_SAVE_DUPS is set to 0
   if [[ ${PHIST_SAVE_DUPS:-1} -eq 0 ]] && [[ $command_part == $PERSISTENT_HISTORY_LAST ]]
   then
      unset PHIST_METADATA
      return
   fi

   # Classic text file
   local DELIM='|'
   echo "$date_part $DELIM $PWD $DELIM $LAST_RC $DELIM $command_part" >> ~/.persistent_history

   # JSON rich file

   # PHIST_METADATA is an associative array that might already contain metadata
   # key value pairs. Redeclaring will not overwrite.
   declare -gA PHIST_METADATA

   local metadata=()
   local metakey metavalue
   for metakey in "${!PHIST_METADATA[@]}"
   do
      metavalue="${PHIST_METADATA[$metakey]}"
      metadata=( "${metadata[@]}" '--metadata' "$metakey" "$metavalue" )
   done
   /home/jacobo.devera/comms/Dropbox/devel/myforge/projects/phist/ph-tool insert \
            "${metadata[@]}"        \
            "$command_part"         \
            "$date_part"            \
            "$PWD"                  \
            "$$"                    \
            "$LAST_RC"              \
            "${#LAST_PIPESTATUS[@]}"   \
            "${LAST_PIPESTATUS[@]}" \
            >> ~/.persistent_history.json

   export PERSISTENT_HISTORY_LAST="$command_part"
   unset PHIST_METADATA
}

persistent_history_add_metadata()
{
   declare -gA PHIST_METADATA
   PHIST_METADATA["$1"]="$2"
}


phack()
{
   ack "$@" ~/.persistent_history
}

color_phist()
{
   colout '^([^\|]+)(\|)([^|]+)(\|) (?:(\?)|(0)|([1-9]\d*)) (\|)(.*)$' 208,21,238,21,yellow,green,red,21,174 normal,bold,normal,bold,bold,bold,bold,bold,bold |
      less -RMFX

}

phist()
{
   local n
   [[ -n $1 ]] && n="-n $1"
   tail $n ~/.persistent_history |
      color_phist
}

# vim: ft=sh fdm=marker expandtab ts=3 sw=3 :

